// Semantic MNIST (Experiment series 4/hard)
// The generator program
// This program generates images that contains less than 10 points.
//
// Copyright (C) 2019 Norbert Bátfai, batfai.norbert@inf.unideb.hu
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
// Version history
//
// Initial hack, the output are binary compatible with the format of
// original MNIST (http://yann.lecun.com/exdb/mnist/) training and test data.
//
// See also https://bhaxor.blog.hu/2019/03/10/the_semantic_mnist and
// http://smartcity.inf.unideb.hu/~norbi/SMNIST
//
// g++ smnistg-hard-disjunct-test-set-no-centering.cpp -o smnistg -lpng
// ./smnistg
//
// bug: 1544 of 60000 train images contain points that have the same coords
//
//  int found = 0;
//
//    for ( int j {0}; j < sem_value; ++j ) {
//
//        x[j] = coords_uniform_dist ( re );
//        y[j] = coords_uniform_dist ( re );
//
//        for ( int k {0}; k < j; ++k ) {
//
//            if ( x[k] == x[j] && y[k] == y[j] && !found ) {
//
//                found = 1;
//                std::cout << " the same point" << std::endl;
//                break;
//
//            }
//        }
//
//        sumx += x[j];
//        sumy += y[j];
//    }
//
// fixed.
//
// At Viktor Simkó's suggestion, the random images that have already been in the training set
// has been filtered out from the test set. This code satisfied a stronger property that what
// Viktor gave, namely all generated random images are unique images. It follows that training
// images are excluded from the test images.
//
// g++ smnistg-hard-disjunct-test-set-no-centering.cpp -o smnistg -lpng
// ./smnistg
//
// "hard": the set of coordinate pairs of pixels is divided into two disjunct sets by the
// function void init_drnd(double p_train). Then the training images are generated from the
// one set and the test images are generated from the other set.
//
// "hard 1px": In the former datasets, dots are 3x3 pixels, now they are 1 pixel
//
// Series 2
//
// g++ smnistg-s2-hard.cpp -lpng -o smnistg2
// ./smnistg2 5 10 20 10
//
// The case of 0 dots is handled standalone
//
// g++ smnistg-s2-hard-pow102x.cpp -lpng -o smnistg2
// ./smnistg2 5 10 20 10
// ./smnistg2 9 10 60000 10000
//
// bugfix:   init_drnd ( 1.0 - 1.0/7.0, size );
//
// g++-8 smnistg-s2-hard-pow102x.cpp -lpng -std=c++17 -lstdc++fs -o smnistg2
//
// ./smnistg2 3 10 600000 10000 >log
// ./smnistg2 2 10 6000 225 
//
// g++-8 smnistg-s4-hard-pow10x+.cpp -lpng -std=c++17 -lstdc++fs -o smnistg2
// ./smnistg2 3 10 60000 10000
// ./smnistg2 4 10 60000 10000
// ./smnistg2 5 10 60000 10000
// ./smnistg2 6 10 60000 10000

#include <iostream>
#include "png++/png.hpp"
#include <random>
#include <fstream>
#include <arpa/inet.h>
#include <map>
#include <array>
#include <vector>
#include <cmath>
#include <filesystem>

const int max_digit_limit = 9;

template< size_t N > using GeneratedImages = std::map<int, std::vector<std::array<int, N> >>;

int max_digit = 9;
uint32_t size = 28;

std::random_device rd;
//std::default_random_engine re ( rd() );
std::default_random_engine re; // debug
std::uniform_real_distribution < float > p_dist ( 0.0, 1.0 );

double pow102x_arr[max_digit_limit];

void init_pow102x()
{
  for ( int i {0}; i<max_digit; ++i )
    {
      pow102x_arr[i] = std::pow ( 10.0, 2.0* ( i+1 ) ) /std::pow ( 10.0, 2.0* ( max_digit ) );
    }

}

int sem_value_pow102x_dist ( std::uniform_real_distribution < double > & pow102x_dist )
{
  int ret = max_digit;
  double r = pow102x_dist ( re );

  for ( int i {1}; i<max_digit; ++i )
    {
      if ( r <= pow102x_arr[i-1] )
        {
          ret = i;
          break;
        }

    }

  return ret;
}

bool little_endian()
{
  uint32_t u = 1;
  char *p = ( char * ) &u;
  return  *p == 1;
}

void write_int ( std::fstream & f, uint32_t n )
{
  if ( little_endian() )
    {
      n = htonl ( n );
    }
  f.write ( ( char * ) &n, sizeof ( uint32_t ) );
}

void write_image ( std::fstream & f, png::image < png::rgb_pixel > & img )
{
  uint8_t pixel;
  int size = img.get_width();

  for ( int j {0}; j < size; ++j )
    for ( int k {0}; k < size; ++k )
      if ( img.get_pixel ( j, k ).red > 0 )
        {
          pixel = 0x00;
          f.write ( ( char * ) &pixel, sizeof ( uint8_t ) );
        }
      else
        {
          pixel = 0xff;
          f.write ( ( char * ) &pixel, sizeof ( uint8_t ) );
        }

}

std::vector<int> drnd_test;
std::vector<int> drnd_train;

void init_drnd ( double p_train, int size )
{

// hard
//std::uniform_int_distribution < int > coords_uniform_dist ( 0 + 4, size - 4 );

  for ( int j {0}; j <= size-1; ++j )
    for ( int k {0}; k <= size-1; ++k )
      if ( p_dist ( re ) < p_train )
        {
          drnd_train.push_back ( j );
          drnd_train.push_back ( k );
        }
      else
        {
          drnd_test.push_back ( j );
          drnd_test.push_back ( k );
        }

}

void coords_drnd ( int *x, int *y, std::vector<int> & rnd_coords )
{
  std::uniform_int_distribution < int > coords_hdisjunct_dist ( 0, rnd_coords.size() /2 -1 );

  int idx = coords_hdisjunct_dist ( re );
  *x = rnd_coords[2*idx];
  *y = rnd_coords[2*idx + 1];
}

GeneratedImages<max_digit_limit*2> generated_images;

bool was_it_already ( int sem_value, int* x, int* y )
{
  bool wasItAlready {false};
  std::array<int, max_digit_limit*2> arr;

  for ( int i {0}; i<sem_value; ++i )
    {
      arr[i*2] = x[i];
      arr[i*2+1] = y[i];
    }

  for ( auto img : generated_images[sem_value] )
    {
      if ( arr == img )
        {
          wasItAlready = true;
          break;
        }
    }

  if ( !wasItAlready )
    {
      generated_images[sem_value].push_back ( arr );
    }

  return wasItAlready;
}

png::image < png::rgb_pixel > create_img ( int size, int sem_value, std::vector<int> & rnd_coords )
{
  png::image < png::rgb_pixel > img ( size, size );

  for ( int j {0}; j < size; ++j )
    for ( int k {0}; k < size; ++k )
      {
        img.set_pixel ( j, k, png::rgb_pixel ( 255, 255, 255 ) );
      }

  if ( !sem_value )
    {
      return img;
    }

  int x[sem_value];
  int y[sem_value];

// no-centering
//    int sumx = 0;
//    int sumy = 0;

  int itWasAlreadyCnt {0};
  do
    {

      for ( int j {0}; j < sem_value; ++j )
        {

          int ok = 1;

          do
            {

// hard
//            x[j] = coords_uniform_dist ( re );
//            y[j] = coords_uniform_dist ( re );

              coords_drnd ( x+j, y+j, rnd_coords );

              ok = 1;
              for ( int k {0}; k < j; ++k )
                {

                  if ( x[k] == x[j] && y[k] == y[j] )
                    {

                      ok = 0;
                    }

                }

            }
          while ( !ok );

// no-centering
//       sumx += x[j];
//       sumy += y[j];
        }


      if ( ++itWasAlreadyCnt > 10 )
        {
          throw std::domain_error ( std::string ( "The domain of images of "+ std::to_string ( sem_value ) + " dot(s) is probably full." ) );
        }

    }
  while ( was_it_already ( sem_value, x, y ) );


// no-centering
//   int meanx = sumx/sem_value;
//   int meany = sumy/sem_value;

//   int xshift = size/2 - meanx;
//   int yshift = size/2 - meany;

  for ( int j {0}; j < sem_value; ++j )
    {

// 1px
//        for ( int k {-1}; k < 2; ++k )
//            for ( int l {-1}; l < 2; ++l ) {
//
//                img.set_pixel ( x[j]+k, y[j]+l, png::rgb_pixel ( 0, 0, 0 ) );

      img.set_pixel ( x[j], y[j], png::rgb_pixel ( 0, 0, 0 ) );

// no-centering
//                if ( x[j]+xshift+k>=0 && x[j]+xshift+k<size
//                        && y[j]+yshift+l>=0 && y[j]+yshift+l<size ) {
//                    img.set_pixel ( x[j]+xshift+k, y[j]+yshift+l, png::rgb_pixel ( 0, 0, 0 ) );
//                } else {
//                    img.set_pixel ( x[j]+k, y[j]+l, png::rgb_pixel ( 0, 0, 0 ) );
//                }


//            }

    }

  return img;
}

void
smnist ( char const *argv[], uint32_t number_of_images,
         std::uniform_real_distribution < double > & pow102x_dist,
         std::vector<int> & rnd_coords,
         std::uniform_int_distribution < int >* sem_value_uniform_dist,
         bool train )

{
  std::string png_files_prefix {"pngs/"};

  if ( !std::filesystem::exists ( png_files_prefix ) )
    std::filesystem::create_directory ( png_files_prefix );

  uint32_t mn_l = 0x00000801;
  uint32_t mn_i = 0x00000803;
  uint8_t label;

  std::fstream labels ( argv[1], std::ios_base::out );
  std::fstream images ( argv[2], std::ios_base::out );

  write_int ( labels, mn_l );
  write_int ( labels, number_of_images );

  write_int ( images, mn_i );
  write_int ( images, number_of_images );
  write_int ( images, size );
  write_int ( images, size );

  const int img_name_size = 512;
  char img_name[img_name_size];

// the case of 0 dots handled standalone
  uint32_t i {0};

  png::image < png::rgb_pixel > img = create_img ( size, 0, rnd_coords );

  write_image ( images, img );

  std::sprintf ( img_name, "%s%s-%d-%d.png", png_files_prefix.c_str(), argv[0], 0, i );
  img.write ( img_name );

  label = 0;
  labels.write ( ( char * ) &label, sizeof ( uint8_t ) );

  std::cout << "\r" << img_name << " saved" << std::flush;

  ++i;

  std::map<int, int> hist;
  ++hist[0];

  //for ( uint32_t i {0}; i < number_of_images; ++i ) {
  //uint32_t i {0};
  while ( i < number_of_images )
    {

// pow102x
//      int sem_value = sem_value_uniform_dist ( re );
//      int sem_value = sem_value_pow102x_dist ( pow102x_dist );

        int sem_value;
        
        /*
      if ( sem_value_uniform_dist == nullptr )
        sem_value = sem_value_pow102x_dist ( pow102x_dist );
      else
        sem_value = ( *sem_value_uniform_dist ) ( re );
        */
        if(train)
        {
            
            if(i<number_of_images/10)
                sem_value = (*sem_value_uniform_dist) ( re );
            else
                sem_value = sem_value_pow102x_dist ( pow102x_dist );                
                
        }
        else
        {
            sem_value = (*sem_value_uniform_dist) ( re );
        }
        
      try
        {

          png::image < png::rgb_pixel > img = create_img ( size, sem_value, rnd_coords );

          write_image ( images, img );

          std::sprintf ( img_name, "%s%s-%d-%d.png", png_files_prefix.c_str(), argv[0], sem_value, i );
          img.write ( img_name );

          label = sem_value;
          labels.write ( ( char * ) &label, sizeof ( uint8_t ) );

          std::cout << "\r" << img_name << " saved" << std::flush;

          ++i;
          ++hist[sem_value];

        }
      catch ( const std::domain_error& e )
        {

          std::cout << e.what() << std::endl;
          // TODO: remove this sem_value from sem_value_uniform_dist
          
          std::cout << sem_value << " " << hist[sem_value] << std::endl;            
          
        }

    }

  std::cout << std::endl << "Stat: " << std::endl;
  for ( auto h : hist )
    std::cout << h.first << " " << h.second << std::endl;

  labels.close();
  images.close();
}

int
main ( int argc, char* argv[] )
{

  max_digit = atoi ( argv[1] );
  if ( max_digit > max_digit_limit )
    max_digit = max_digit_limit;

  size = atoi ( argv[2] );
  uint32_t number_of_train_images = atoi ( argv[3] );
  uint32_t number_of_test_images = atoi ( argv[4] );

// the case of 0 dots handled standalone
//    std::uniform_int_distribution < int > sem_value_uniform_dist ( 0, max_digit );
  std::uniform_int_distribution < int > sem_value_uniform_dist ( 1, max_digit );
  std::uniform_real_distribution < double > pow102x_dist ( 0.0, 1.0 );

  //
  //init_drnd ( 1.0-1.0/8.0, size );
  // s3-3hardp102x
  init_drnd ( 1.0-1.0/8.0, size );
  
  std::cout <<  drnd_train.size() /2 << std::endl;
  std::cout <<  drnd_test.size() /2 << std::endl;

  init_pow102x();

  char const *train[]=
  {
    "smnistg-train", "train-labels-idx1-ubyte", "train-images-idx3-ubyte"
  };

  smnist ( train, number_of_train_images, pow102x_dist, drnd_train, &sem_value_uniform_dist, true );

  char const *test[]=
  {
    "smnistg-test", "t10k-labels-idx1-ubyte", "t10k-images-idx3-ubyte"
  };

  smnist ( test, number_of_test_images, pow102x_dist, drnd_test, &sem_value_uniform_dist, false );

}



