 The Semantic "MNIST" - Counting the Number of Dots in an Image
==

 Have you seen the movie, Rain Man? Do you remember the scene when Dustin Hoffman can count the exact number of toothpicks on the floor in the blink of an eye. This scene gave the idea to implement it as a machine learning example. We don't count toothpicks but dots in an image.

 For further details see the blog post: [https://bhaxor.blog.hu/2019/03/10/the_semantic_mnist](https://bhaxor.blog.hu/2019/03/10/the_semantic_mnist)  
 The sources can be found at [https://gitlab.com/nbatfai/smnist](https://gitlab.com/nbatfai/smnist)  
Here is an introductory video: [https://youtu.be/-tSRwJgVpJk](https://youtu.be/-tSRwJgVpJk)

# SMNIST for Humans

The experiments called **SMNIST for Humans** are intended to measure the capacity of the parallel individuation system in humans.

[Experiment 3](http://smartcity.inf.unideb.hu/~norbi/SMNIST/SMNISTforHUMANS/Exp3/)


# SMNIST for Machines

The experiments called **SMNIST for Machines** are similar to previous ones but they investigate computer programs.


# Downloads

## Series 1

### Naive

* [t10k-images-idx3-ubyte.gz](naive/t10k-images-idx3-ubyte.gz)
* [t10k-labels-idx1-ubyte.gz](naive/t10k-labels-idx1-ubyte.gz)
* [t10k-images-rgb-png.tgz](naive/t10k-images-rgb-png.tgz)


* [train-images-idx3-ubyte.gz](naive/train-images-idx3-ubyte.gz)
* [train-labels-idx1-ubyte.gz](naive/train-labels-idx1-ubyte.gz)
* [train-images-rgb-png.tgz](naive/train-images-rgb-png.tgz)


Source: [smnistg.cpp](https://gitlab.com/nbatfai/smnist/blob/master/smnistg/smnistg.cpp)

### No-centering

* [t10k-images-idx3-ubyte.gz](no-centering/t10k-images-idx3-ubyte.gz)
* [t10k-labels-idx1-ubyte.gz](no-centering/t10k-labels-idx1-ubyte.gz)
* [t10k-images-rgb-png.tgz](no-centering/t10k-images-rgb-png.tgz)


* [train-images-idx3-ubyte.gz](no-centering/train-images-idx3-ubyte.gz)
* [train-labels-idx1-ubyte.gz](no-centering/train-labels-idx1-ubyte.gz)
* [train-images-rgb-png.tgz](no-centering/train-images-rgb-png.tgz)


Source: [smnistg-no-centering.cpp](https://gitlab.com/nbatfai/smnist/blob/master/smnistg/smnistg-no-centering.cpp)


### Disjunct

* [t10k-images-idx3-ubyte.gz](disjunct/t10k-images-idx3-ubyte.gz)
* [t10k-labels-idx1-ubyte.gz](disjunct/t10k-labels-idx1-ubyte.gz)
* [t10k-images-rgb-png.tgz](disjunct/t10k-images-rgb-png.tgz)


* [train-images-idx3-ubyte.gz](disjunct/train-images-idx3-ubyte.gz)
* [train-labels-idx1-ubyte.gz](disjunct/train-labels-idx1-ubyte.gz)
* [train-images-rgb-png.tgz](disjunct/train-images-rgb-png.tgz)


Source: [smnistg-disjunct-test-set-no-centering.cpp](https://gitlab.com/nbatfai/smnist/blob/master/smnistg/smnistg-disjunct-test-set-no-centering.cpp)


### Disjunct 1PX

* [t10k-images-idx3-ubyte.gz](disjunct1px/t10k-images-idx3-ubyte.gz)
* [t10k-labels-idx1-ubyte.gz](disjunct1px/t10k-labels-idx1-ubyte.gz)
* [t10k-images-rgb-png.tgz](disjunct1px/t10k-images-rgb-png.tgz)


* [train-images-idx3-ubyte.gz](disjunct1px/train-images-idx3-ubyte.gz)
* [train-labels-idx1-ubyte.gz](disjunct1px/train-labels-idx1-ubyte.gz)
* [train-images-rgb-png.tgz](disjunct1px/train-images-rgb-png.tgz)


Source: [smnistg-disjunct-1px-test-set-no-centering.cpp](https://gitlab.com/nbatfai/smnist/blob/master/smnistg/smnistg-disjunct-1px-test-set-no-centering.cpp)



### Hard

* [t10k-images-idx3-ubyte.gz](hard/t10k-images-idx3-ubyte.gz)
* [t10k-labels-idx1-ubyte.gz](hard/t10k-labels-idx1-ubyte.gz)
* [t10k-images-rgb-png.tgz](hard/t10k-images-rgb-png.tgz)


* [train-images-idx3-ubyte.gz](hard/train-images-idx3-ubyte.gz)
* [train-labels-idx1-ubyte.gz](hard/train-labels-idx1-ubyte.gz)
* [train-images-rgb-png.tgz](hard/train-images-rgb-png.tgz)


Source: [smnistg-hard-disjunct-test-set-no-centering.cpp](https://gitlab.com/nbatfai/smnist/blob/master/smnistg/smnistg-hard-disjunct-test-set-no-centering.cpp)



### Hard 1PX

* [t10k-images-idx3-ubyte.gz](hard1px/t10k-images-idx3-ubyte.gz)
* [t10k-labels-idx1-ubyte.gz](hard1px/t10k-labels-idx1-ubyte.gz)
* [t10k-images-rgb-png.tgz](hard1px/t10k-images-rgb-png.tgz)


* [train-images-idx3-ubyte.gz](hard1px/train-images-idx3-ubyte.gz)
* [train-labels-idx1-ubyte.gz](hard1px/train-labels-idx1-ubyte.gz)
* [train-images-rgb-png.tgz](hard1px/train-images-rgb-png.tgz)


Source: [smnistg-hard-1px-disjunct-test-set-no-centering.cpp](https://gitlab.com/nbatfai/smnist/blob/master/smnistg/smnistg-hard-1px-disjunct-test-set-no-centering.cpp)


## Series 2


### Disjunct 10x10, 1PX


*  [t10k-images-idx3-ubyte.gz](s2disjunct/t10k-images-idx3-ubyte.gz)
*  [t10k-labels-idx1-ubyte.gz](s2disjunct/t10k-labels-idx1-ubyte.gz)
*  [t10k-images-rgb-png.tgz](s2disjunct/t10k-images-rgb-png.tgz)


*  [train-images-idx3-ubyte.gz](s2disjunct/train-images-idx3-ubyte.gz)
*  [train-labels-idx1-ubyte.gz](s2disjunct/train-labels-idx1-ubyte.gz)
*  [train-images-rgb-png.tgz](s2disjunct/train-images-rgb-png.tgz)



Source: [smnistg-s2-disjunct.cpp](https://gitlab.com/nbatfai/smnist/blob/master/smnistg-s2/smnistg-s2-disjunct.cpp)



### Hard 10x10, 1PX

*  [t10k-images-idx3-ubyte.gz](s2hard/t10k-images-idx3-ubyte.gz)
*  [t10k-labels-idx1-ubyte.gz](s2hard/t10k-labels-idx1-ubyte.gz)
*  [t10k-images-rgb-png.tgz](s2hard/t10k-images-rgb-png.tgz)


*  [train-images-idx3-ubyte.gz](s2hard/train-images-idx3-ubyte.gz)
*  [train-labels-idx1-ubyte.gz](s2hard/train-labels-idx1-ubyte.gz)
*  [train-images-rgb-png.tgz](s2hard/train-images-rgb-png.tgz)


Source: [smnistg-s2-hard.cpp](https://gitlab.com/nbatfai/smnist/blob/master/smnistg-s2/smnistg-s2-hard.cpp)


### Disjunct 10x10, 1PX, pow 10 2x+

*  [t10k-images-idx3-ubyte.gz](s2disjunctp102x/t10k-images-idx3-ubyte.gz)
*  [t10k-labels-idx1-ubyte.gz](s2disjunctp102x/t10k-labels-idx1-ubyte.gz)
*  [t10k-images-rgb-png.tgz](s2disjunctp102x/t10k-images-rgb-png.tgz)


*  [train-images-idx3-ubyte.gz](s2disjunctp102x/train-images-idx3-ubyte.gz)
*  [train-labels-idx1-ubyte.gz](s2disjunctp102x/train-labels-idx1-ubyte.gz)
*  [train-images-rgb-png.tgz](s2disjunctp102x/train-images-rgb-png.tgz)


Source: [smnistg-s2-disjunct-pow102x+.cpp](https://gitlab.com/nbatfai/smnist/blob/master/smnistg-s2/smnistg-s2-disjunct-pow102x+.cpp)


### Hard 10x10, 1PX pow 10 2x+


*  [t10k-images-idx3-ubyte.gz](s2hardp102x/t10k-images-idx3-ubyte.gz)
*  [t10k-labels-idx1-ubyte.gz](s2hardp102x/t10k-labels-idx1-ubyte.gz)
*  [t10k-images-rgb-png.tgz](s2hardp102x/t10k-images-rgb-png.tgz)


*  [train-images-idx3-ubyte.gz](s2hardp102x/train-images-idx3-ubyte.gz)
*  [train-labels-idx1-ubyte.gz](s2hardp102x/train-labels-idx1-ubyte.gz)
*  [train-images-rgb-png.tgz](s2hardp102x/train-images-rgb-png.tgz)


Source: [smnistg-s2-hard-pow102x+.cpp](https://gitlab.com/nbatfai/smnist/blob/master/smnistg-s2/smnistg-s2-hard-pow102x+.cpp)


#### Hard 10x10, 1PX pow 10 2x+, m=4

*  [t10k-images-idx3-ubyte.gz](s2-4-hardp102x/t10k-images-idx3-ubyte.gz)
*  [t10k-labels-idx1-ubyte.gz](s2-4-hardp102x/t10k-labels-idx1-ubyte.gz)
*  [t10k-images-rgb-png.tgz](s2-4-hardp102x/t10k-images-rgb-png.tgz)


*  [train-images-idx3-ubyte.gz](s2-4-hardp102x/train-images-idx3-ubyte.gz)
*  [train-labels-idx1-ubyte.gz](s2-4-hardp102x/train-labels-idx1-ubyte.gz)
*  [train-images-rgb-png.tgz](s2-4-hardp102x/train-images-rgb-png.tgz)


#### Hard 10x10, 1PX pow 10 2x+, m=5

*  [t10k-images-idx3-ubyte.gz](s2-5-hardp102x/t10k-images-idx3-ubyte.gz)
*  [t10k-labels-idx1-ubyte.gz](s2-5-hardp102x/t10k-labels-idx1-ubyte.gz)
*  [t10k-images-rgb-png.tgz](s2-5-hardp102x/t10k-images-rgb-png.tgz)


*  [train-images-idx3-ubyte.gz](s2-5-hardp102x/train-images-idx3-ubyte.gz)
*  [train-labels-idx1-ubyte.gz](s2-5-hardp102x/train-labels-idx1-ubyte.gz)
*  [train-images-rgb-png.tgz](s2-5-hardp102x/train-images-rgb-png.tgz)

#### Hard 10x10, 1PX pow 10 2x+, m=6

*  [t10k-images-idx3-ubyte.gz](s2-6-hardp102x/t10k-images-idx3-ubyte.gz)
*  [t10k-labels-idx1-ubyte.gz](s2-6-hardp102x/t10k-labels-idx1-ubyte.gz)
*  [t10k-images-rgb-png.tgz](s2-6-hardp102x/t10k-images-rgb-png.tgz)


*  [train-images-idx3-ubyte.gz](s2-6-hardp102x/train-images-idx3-ubyte.gz)
*  [train-labels-idx1-ubyte.gz](s2-6-hardp102x/train-labels-idx1-ubyte.gz)
*  [train-images-rgb-png.tgz](s2-6-hardp102x/train-images-rgb-png.tgz)

#### Hard 10x10, 1PX pow 10 2x+, m=7

*  [t10k-images-idx3-ubyte.gz](s2-7-hardp102x/t10k-images-idx3-ubyte.gz)
*  [t10k-labels-idx1-ubyte.gz](s2-7-hardp102x/t10k-labels-idx1-ubyte.gz)
*  [t10k-images-rgb-png.tgz](s2-7-hardp102x/t10k-images-rgb-png.tgz)


*  [train-images-idx3-ubyte.gz](s2-7-hardp102x/train-images-idx3-ubyte.gz)
*  [train-labels-idx1-ubyte.gz](s2-7-hardp102x/train-labels-idx1-ubyte.gz)
*  [train-images-rgb-png.tgz](s2-7-hardp102x/train-images-rgb-png.tgz)

#### Hard 10x10, 1PX pow 10 2x+, m=8

*  [t10k-images-idx3-ubyte.gz](s2-8-hardp102x/t10k-images-idx3-ubyte.gz)
*  [t10k-labels-idx1-ubyte.gz](s2-8-hardp102x/t10k-labels-idx1-ubyte.gz)
*  [t10k-images-rgb-png.tgz](s2-8-hardp102x/t10k-images-rgb-png.tgz)


*  [train-images-idx3-ubyte.gz](s2-8-hardp102x/train-images-idx3-ubyte.gz)
*  [train-labels-idx1-ubyte.gz](s2-8-hardp102x/train-labels-idx1-ubyte.gz)
*  [train-images-rgb-png.tgz](s2-8-hardp102x/train-images-rgb-png.tgz)


# SMNIST for Anyone

# Downloads

## Series 1

### Hard 10x10, 1PX pow 10 2x+, m=4-9, 60.000/10.000
Source: [smnistg-sX5-hard-pow10x+.cpp](https://gitlab.com/nbatfai/smnist/blob/master/smnistg-s3/smnistg-sX5-hard-pow10x+.cpp)

#### m=4

* [t10k-images-idx3-ubyte.gz](r2s1-4-hard-pow10x+/t10k-images-idx3-ubyte.gz)
* [t10k-labels-idx1-ubyte.gz](r2s1-4-hard-pow10x+/t10k-labels-idx1-ubyte.gz)
* [t10k-images-rgb-png.tgz](r2s1-4-hard-pow10x+/t10k-images-rgb-png.tgz)


* [train-images-idx3-ubyte.gz](r2s1-4-hard-pow10x+/train-images-idx3-ubyte.gz)
* [train-labels-idx1-ubyte.gz](r2s1-4-hard-pow10x+/train-labels-idx1-ubyte.gz)
* [train-images-rgb-png.tgz](r2s1-4-hard-pow10x+/train-images-rgb-png.tgz)


#### m=5

* [t10k-images-idx3-ubyte.gz](r2s1-5-hard-pow10x+/t10k-images-idx3-ubyte.gz)
* [t10k-labels-idx1-ubyte.gz](r2s1-5-hard-pow10x+/t10k-labels-idx1-ubyte.gz)
* [t10k-images-rgb-png.tgz](r2s1-5-hard-pow10x+/t10k-images-rgb-png.tgz)


* [train-images-idx3-ubyte.gz](r2s1-5-hard-pow10x+/train-images-idx3-ubyte.gz)
* [train-labels-idx1-ubyte.gz](r2s1-5-hard-pow10x+/train-labels-idx1-ubyte.gz)
* [train-images-rgb-png.tgz](r2s1-5-hard-pow10x+/train-images-rgb-png.tgz)


#### m=6

* [t10k-images-idx3-ubyte.gz](r2s1-6-hard-pow10x+/t10k-images-idx3-ubyte.gz)
* [t10k-labels-idx1-ubyte.gz](r2s1-6-hard-pow10x+/t10k-labels-idx1-ubyte.gz)
* [t10k-images-rgb-png.tgz](r2s1-6-hard-pow10x+/t10k-images-rgb-png.tgz)


* [train-images-idx3-ubyte.gz](r2s1-6-hard-pow10x+/train-images-idx3-ubyte.gz)
* [train-labels-idx1-ubyte.gz](r2s1-6-hard-pow10x+/train-labels-idx1-ubyte.gz)
* [train-images-rgb-png.tgz](r2s1-6-hard-pow10x+/train-images-rgb-png.tgz)

#### m=7

* [t10k-images-idx3-ubyte.gz](r2s1-7-hard-pow10x+/t10k-images-idx3-ubyte.gz)
* [t10k-labels-idx1-ubyte.gz](r2s1-7-hard-pow10x+/t10k-labels-idx1-ubyte.gz)
* [t10k-images-rgb-png.tgz](r2s1-7-hard-pow10x+/t10k-images-rgb-png.tgz)


* [train-images-idx3-ubyte.gz](r2s1-7-hard-pow10x+/train-images-idx3-ubyte.gz)
* [train-labels-idx1-ubyte.gz](r2s1-7-hard-pow10x+/train-labels-idx1-ubyte.gz)
* [train-images-rgb-png.tgz](r2s1-7-hard-pow10x+/train-images-rgb-png.tgz)

#### m=8

* [t10k-images-idx3-ubyte.gz](r2s1-8-hard-pow10x+/t10k-images-idx3-ubyte.gz)
* [t10k-labels-idx1-ubyte.gz](r2s1-8-hard-pow10x+/t10k-labels-idx1-ubyte.gz)
* [t10k-images-rgb-png.tgz](r2s1-8-hard-pow10x+/t10k-images-rgb-png.tgz)


* [train-images-idx3-ubyte.gz](r2s1-8-hard-pow10x+/train-images-idx3-ubyte.gz)
* [train-labels-idx1-ubyte.gz](r2s1-8-hard-pow10x+/train-labels-idx1-ubyte.gz)
* [train-images-rgb-png.tgz](r2s1-8-hard-pow10x+/train-images-rgb-png.tgz)

#### m=9

* [t10k-images-idx3-ubyte.gz](r2s1-9-hard-pow10x+/t10k-images-idx3-ubyte.gz)
* [t10k-labels-idx1-ubyte.gz](r2s1-9-hard-pow10x+/t10k-labels-idx1-ubyte.gz)
* [t10k-images-rgb-png.tgz](r2s1-9-hard-pow10x+/t10k-images-rgb-png.tgz)


* [train-images-idx3-ubyte.gz](r2s1-9-hard-pow10x+/train-images-idx3-ubyte.gz)
* [train-labels-idx1-ubyte.gz](r2s1-9-hard-pow10x+/train-labels-idx1-ubyte.gz)
* [train-images-rgb-png.tgz](r2s1-9-hard-pow10x+/train-images-rgb-png.tgz)



## Series 2

### Hard 10x10, 1PX pow 10 2x+, m=4-9, 60.000/10.000
Source: [smnistg-s5-hard-pow10x+.cpp](https://gitlab.com/nbatfai/smnist/blob/master/smnistg-s3/smnistg-s5-hard-pow10x+.cpp)

#### m=4

* [t10k-images-idx3-ubyte.gz](r2s2-4-hard-pow10x+/t10k-images-idx3-ubyte.gz)
* [t10k-labels-idx1-ubyte.gz](r2s2-4-hard-pow10x+/t10k-labels-idx1-ubyte.gz)
* [t10k-images-rgb-png.tgz](r2s2-4-hard-pow10x+/t10k-images-rgb-png.tgz)


* [train-images-idx3-ubyte.gz](r2s2-4-hard-pow10x+/train-images-idx3-ubyte.gz)
* [train-labels-idx1-ubyte.gz](r2s2-4-hard-pow10x+/train-labels-idx1-ubyte.gz)
* [train-images-rgb-png.tgz](r2s2-4-hard-pow10x+/train-images-rgb-png.tgz)


#### m=5

* [t10k-images-idx3-ubyte.gz](r2s2-5-hard-pow10x+/t10k-images-idx3-ubyte.gz)
* [t10k-labels-idx1-ubyte.gz](r2s2-5-hard-pow10x+/t10k-labels-idx1-ubyte.gz)
* [t10k-images-rgb-png.tgz](r2s2-5-hard-pow10x+/t10k-images-rgb-png.tgz)


* [train-images-idx3-ubyte.gz](r2s2-5-hard-pow10x+/train-images-idx3-ubyte.gz)
* [train-labels-idx1-ubyte.gz](r2s2-5-hard-pow10x+/train-labels-idx1-ubyte.gz)
* [train-images-rgb-png.tgz](r2s2-5-hard-pow10x+/train-images-rgb-png.tgz)


#### m=6

* [t10k-images-idx3-ubyte.gz](r2s2-6-hard-pow10x+/t10k-images-idx3-ubyte.gz)
* [t10k-labels-idx1-ubyte.gz](r2s2-6-hard-pow10x+/t10k-labels-idx1-ubyte.gz)
* [t10k-images-rgb-png.tgz](r2s2-6-hard-pow10x+/t10k-images-rgb-png.tgz)


* [train-images-idx3-ubyte.gz](r2s2-6-hard-pow10x+/train-images-idx3-ubyte.gz)
* [train-labels-idx1-ubyte.gz](r2s2-6-hard-pow10x+/train-labels-idx1-ubyte.gz)
* [train-images-rgb-png.tgz](r2s2-6-hard-pow10x+/train-images-rgb-png.tgz)

#### m=7

* [t10k-images-idx3-ubyte.gz](r2s2-7-hard-pow10x+/t10k-images-idx3-ubyte.gz)
* [t10k-labels-idx1-ubyte.gz](r2s2-7-hard-pow10x+/t10k-labels-idx1-ubyte.gz)
* [t10k-images-rgb-png.tgz](r2s2-7-hard-pow10x+/t10k-images-rgb-png.tgz)


* [train-images-idx3-ubyte.gz](r2s2-7-hard-pow10x+/train-images-idx3-ubyte.gz)
* [train-labels-idx1-ubyte.gz](r2s2-7-hard-pow10x+/train-labels-idx1-ubyte.gz)
* [train-images-rgb-png.tgz](r2s2-7-hard-pow10x+/train-images-rgb-png.tgz)

#### m=8

* [t10k-images-idx3-ubyte.gz](r2s2-8-hard-pow10x+/t10k-images-idx3-ubyte.gz)
* [t10k-labels-idx1-ubyte.gz](r2s2-8-hard-pow10x+/t10k-labels-idx1-ubyte.gz)
* [t10k-images-rgb-png.tgz](r2s2-8-hard-pow10x+/t10k-images-rgb-png.tgz)


* [train-images-idx3-ubyte.gz](r2s2-8-hard-pow10x+/train-images-idx3-ubyte.gz)
* [train-labels-idx1-ubyte.gz](r2s2-8-hard-pow10x+/train-labels-idx1-ubyte.gz)
* [train-images-rgb-png.tgz](r2s2-8-hard-pow10x+/train-images-rgb-png.tgz)

#### m=9

* [t10k-images-idx3-ubyte.gz](r2s2-9-hard-pow10x+/t10k-images-idx3-ubyte.gz)
* [t10k-labels-idx1-ubyte.gz](r2s2-9-hard-pow10x+/t10k-labels-idx1-ubyte.gz)
* [t10k-images-rgb-png.tgz](r2s2-9-hard-pow10x+/t10k-images-rgb-png.tgz)


* [train-images-idx3-ubyte.gz](r2s2-9-hard-pow10x+/train-images-idx3-ubyte.gz)
* [train-labels-idx1-ubyte.gz](r2s2-9-hard-pow10x+/train-labels-idx1-ubyte.gz)
* [train-images-rgb-png.tgz](r2s2-9-hard-pow10x+/train-images-rgb-png.tgz)


## Version history

[Bugfix: 1544 of 60000 train images contain points that have the same coords](https://gitlab.com/nbatfai/smnist/commit/34d9d2bde66355dd5468fbf28ec06493cad45754)

[At Viktor Simkó's suggestion, the random images that have already been in the training set has been filtered out from the test set. This code satisfied a stronger property that what Viktor gave, namely all generated random images are unique images. It follows that training images are excluded from the test images.](https://gitlab.com/nbatfai/smnist/commit/86bfebb5255aafc959807e0187e05cfc86605f08)

[hard: the set of coordinate pairs of pixels is divided into two disjunct sets by the function void init_drnd(double p_train). Then the training images are generated from the one set and the test images are generated from the other set.](https://gitlab.com/nbatfai/smnist/commit/b57b527e59b1f0d59c0fa933cc7c70c358e91bdf)

[Regenerating missing images.](https://gitlab.com/nbatfai/smnist/commit/d9d0e1104cc5d75fffc9329685079880794d7130)

["1px": In the former datasets, dots are 3x3 pixels, now they are 1 pixel.](https://gitlab.com/nbatfai/smnist/blob/master/smnistg/smnistg-hard-1px-disjunct-test-set-no-centering.cpp)

[The case of 0 dots is handled standalone.](https://gitlab.com/nbatfai/smnist/commit/3605d9461617235bb4327cb1ade63e00ce03ae33)

[pow 10 2x](https://gitlab.com/nbatfai/smnist/commit/0893b3426b548cca5d015a0da93b0209ee493af2)

[Uniform test set](https://gitlab.com/nbatfai/smnist/commit/e03247a88aa9c3b801904f697790e072fb84cb6b)

Last modified: 13 June 2019

Norbert Bátfai, PhD   
batfai.norbert@inf.unideb.hu   
https://arato.inf.unideb.hu/batfai.norbert/   
IT Dept, UD
