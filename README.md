# SMNIST

The Semantic "MNIST". Have you seen the movie, Rain Man? Do you remember the scene when Dustin Hoffman can count the exact number of toothpicks on the floor in the blink of an eye. We don't count toothpicks but dots in an image. 

For further details see the blog post: [https://bhaxor.blog.hu/2019/03/10/the_semantic_mnist](https://bhaxor.blog.hu/2019/03/10/the_semantic_mnist)

Download: [http://smartcity.inf.unideb.hu/~norbi/SMNIST/](http://smartcity.inf.unideb.hu/~norbi/SMNIST/)

Here is an introductory video: [https://youtu.be/-tSRwJgVpJk](https://youtu.be/-tSRwJgVpJk)


## Publications
* Norbert Bátfai, Dávid Papp, Gergő Bogacsovics, Máté Szabó, Viktor Szilárd Simkó, Márió Bersenszki, 
Gergely Szabó, Lajos Kovács, Ferencz Kovács, Erik Szilveszter Varga (2019) 
**Object file system software experiments about the notion of number in humans and machines**, Cognition, Brain, Behavior. An Interdisciplinary Journal, 
23/4, 257-280, doi:10.24193/cbb.2019.23.15

* Norbert Bátfai, Dávid Papp, Gergő Bogacsovics, Máté Szabó, Viktor Szilárd Simkó, Márió Bersenszki, 
Gergely Szabó, Lajos Kovács, Ferencz Kovács, Erik Szilveszter Varga (2019) **On the notion of number in humans and machines**
[https://arxiv.org/abs/1906.12213](https://arxiv.org/abs/1906.12213)